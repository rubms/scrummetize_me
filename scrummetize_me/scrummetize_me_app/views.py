# Create your views here.

from django.shortcuts import render_to_response, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from scrummetize_me_app.models import Product, Story
from django.http import Http404
from django.template import RequestContext
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User

#def index(request):
#	top_ten_products = Product.objects.all().order_by('-creation_date')[:10]
#	return render_to_response('scrummetize_me_app/index.html', {'top_ten_products' : top_ten_products})

#def story_detail(request, story_id):
##	try:
##		story = Story.objects.get(pk=story_id)
##	except Story.DoesNotExist:
##		raise Http404	
#	story = get_object_or_404(Story, pk=story_id)
#	return render_to_response('scrummetize_me_app/story.html', {'story':story})

def product_list(request):
	user = request.user
	products = Product.objects.filter(productparticipant__participant__id=User.objects.get(username__startswith='ruben').id)#user.id)
	return render_to_response('scrummetize_me_app/product_list.html', {'products' : products})

def new_story(request, product_id):
	return render_to_response('scrummetize_me_app/new_story.html', {'product_id':product_id}, context_instance=RequestContext(request))

def add_story(request, product_id):
	name = request.POST['name']
	description = request.POST['description']
	story_points = int(request.POST['points'])
	if not name:
		return render_to_response('scrummetize_me_app/new_story.html', {'error_message':'You must specify a name for the story'}, context_instance=RequestContext(request))
	try:
		product = get_object_or_404(Product, pk=product_id)
	except (KeyError, Product.DoesNotExist):
		raise Http404
	story = product.backlog.story_set.create(name=name, description=description, estimated_points=story_points)
	return HttpResponseRedirect(reverse('story_detail', args=(story.id)))

