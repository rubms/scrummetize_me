from scrummetize_me_app.models import Product, Backlog, Story, Task, Tag, ProductParticipant
from django.contrib import admin

class StoryInline(admin.TabularInline):
	model = Story
	fields = ['name', 'points']
	extra = 1

class ProductAdmin(admin.ModelAdmin):
	fieldsets = [
		(None, { 'fields' : ['name', 'description', 'backlog'] } ),
		('Date/time information', { 'fields': ['creation_date'], 'classes' : ['collapse'] })
	]
	list_display = ('name', 'creation_date')
	list_filter = ['creation_date']
	search_fields = ['name']
	date_hierarchy = 'creation_date'

#class BacklogAdmin(admin.ModelAdmin):
class BacklogAdmin(admin.ModelAdmin):
	fields = ['name']
	search_fields = ['name']
	inlinines = [StoryInline]
	list_display = ('name', 'story_points')

admin.site.register(Product, ProductAdmin)
admin.site.register(Backlog, BacklogAdmin)
admin.site.register(Story)
admin.site.register(Tag)
admin.site.register(ProductParticipant)
