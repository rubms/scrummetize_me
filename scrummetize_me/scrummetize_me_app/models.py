from django.db import models
from django.contrib.auth.models import User
import datetime
from django.core.validators import MaxValueValidator, MinValueValidator

# Create your models here.
class Tag(models.Model):
	tag = models.CharField(max_length=200)

	def __unicode__(self):
		return self.tag

class Participant(models.Model):
	participant = models.ForeignKey(User)
	
	class Meta:
		abstract = True

class Backlog(models.Model):
	name = models.CharField(max_length=200)

	def __unicode__(self):
		return self.name
		
	def ordered_stories(self):
		return self.story_set.all().order_by('-importance')
		
	def story_points(self):
		stories = self.story_set.all()
		points = 0
		for story in stories:
			points += story.estimated_points

class Product(models.Model):
	name = models.CharField(max_length=200)
	description = models.TextField(blank=True)
	creation_date = models.DateTimeField(default=datetime.datetime.now())
	wiki = models.TextField(blank=True)
	backlog = models.OneToOneField(Backlog, blank=True)

	def __unicode__(self):
		return self.name

class ProductParticipant(Participant):
	product = models.ForeignKey(Product) # Product in which the team member participates

	def __unicode__(self):
		return self.participant.username

class Sprint(models.Model):
	name = models.CharField(max_length=200)
	goal = models.CharField(max_length=400, blank=True)
	start_date = models.DateTimeField(default=datetime.datetime.now())
	end_date = models.DateTimeField()
	wiki = models.TextField(blank=True)
	
	product = models.ForeignKey(Product)
	backlog = models.OneToOneField(Backlog, blank=True)
	
	estimated_speed = models.IntegerField(blank=True)
	
	def __unicode__(self):
		return self.name

class SprintParticipant(Participant):
	participation_rate = models.FloatField(default=100, validators=[MaxValueValidator(100), MinValueValidator(1)]) # Value between 1 and 100.
	sprint = models.ForeignKey(Sprint) # Sprint in which the team member participates

	def __unicode__(self):
		return self.participant.username
	
class Story(models.Model):
	name = models.CharField(max_length=200)
	description = models.TextField(blank=True)
	estimated_points = models.IntegerField(default=0)
	importance = models.IntegerField(default=0)
	how_to_demo = models.TextField(blank=True)

	external_id = models.CharField(max_length=200, blank=True)
	requestor = models.CharField(max_length=200, blank=True)
	
	backlog = models.ForeignKey(Backlog)
	tags = models.ManyToManyField(Tag, blank=True)

	creator = models.ForeignKey(User)
	creation_date = models.DateTimeField(default=datetime.datetime.now())

	def __unicode__(self):
		return self.name

class Task(models.Model):
	name = models.CharField(max_length=200)
	description = models.TextField(blank=True)

	story = models.ForeignKey(Story)
	tags = models.ManyToManyField(Tag, blank=True)

	def __unicode__(self):
		return self.name

class Document(models.Model):
	name = models.CharField(max_length=200)
	wiki = models.TextField(blank=True)
	product = models.ForeignKey(Product)

class Note(models.Model):
	creator = models.ForeignKey(User)
	date = models.DateTimeField(default=datetime.datetime.now())
	note = models.TextField()
	
	def __unicode__(self):
		return self.note; 

	class Meta:
		abstract = True

class StoryNote(Note):
	story = models.ForeignKey(Story)

	def __unicode__(self):
		return self.note; 

class TaskNote(Note):
	task = models.ForeignKey(Task)

	def __unicode__(self):
		return self.note; 

class UploadedFile(models.Model):
	uploaded_file = models.FileField(upload_to='uploaded_files')

	class Meta:
		abstract = True

class StoryUploadedFile(UploadedFile):
	story = models.ForeignKey(Story, blank=True)

class TaskUploadedFile(UploadedFile):
	task = models.ForeignKey(Task, blank=True)

class DocumentUploadedFile(UploadedFile):
	document = models.ForeignKey(Document, blank=True)

