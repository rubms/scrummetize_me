from scrummetize_me_app.models import Product, ProductParticipant, Backlog, Story, Task
from django.contrib.auth.models import User

b = Backlog(name='Product backlog')
b.save()
b.story_set.create(name='Historia 1.1', description='Descripcion de la historia 1', creator=User.objects.get(username__startswith='ruben'))
b.story_set.create(name='Historia 1.2', description='Descripcion de la historia 2', creator=User.objects.get(username__startswith='ruben'))
b.story_set.create(name='Historia 1.3', description='Descripcion de la historia 3', creator=User.objects.get(username__startswith='ruben'))
p = Product(name='Producto de prueba 1', description='Descripcion del producto 1')
p.backlog = b
p.save()
p.productparticipant_set.create(participant=User.objects.get(username__startswith='ruben'))
p.save()

b = Backlog(name='Product backlog')
b.save()
b.story_set.create(name='Historia 2.1', description='Descripcion de la historia 1', creator=User.objects.get(username__startswith='ruben'))
b.story_set.create(name='Historia 2.2', description='Descripcion de la historia 2', creator=User.objects.get(username__startswith='ruben'))
b.story_set.create(name='Historia 2.3', description='Descripcion de la historia 3', creator=User.objects.get(username__startswith='ruben'))
p = Product(name='Producto de prueba 2', description='Descripcion del producto 2')
p.backlog = b
p.save()
p.productparticipant_set.create(participant=User.objects.get(username__startswith='ruben'))
p.save()

b = Backlog(name='Product backlog')
b.save()
b.story_set.create(name='Historia 3.1', description='Descripcion de la historia 1', creator=User.objects.get(username__startswith='ruben'))
b.story_set.create(name='Historia 3.2', description='Descripcion de la historia 2', creator=User.objects.get(username__startswith='ruben'))
b.story_set.create(name='Historia 3.3', description='Descripcion de la historia 3', creator=User.objects.get(username__startswith='ruben'))
p = Product(name='Producto de prueba 3', description='Descripcion del producto 3')
p.backlog = b
p.save()

