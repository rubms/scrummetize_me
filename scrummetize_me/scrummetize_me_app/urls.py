from django.conf.urls.defaults import patterns, include, url
from django.conf.urls.defaults import *
from django.views.generic import DetailView, ListView
from scrummetize_me_app.models import Story, Product, Backlog

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    #url(r'^$', 'scrummetize_me_app.views.index'),
    url(r'^$', ListView.as_view(queryset=Product.objects.order_by('-creation_date')[:10],
            context_object_name='latest_product_list',
            template_name='scrummetize_me_app/index.html')),
    url(r'^product_list$', 'scrummetize_me_app.views.product_list'),
    url(r'^backlog/(?P<pk>\d+)$', DetailView.as_view(model=Backlog)),
    #url(r'^story/(?P<story_id>\d+)/', 'scrummetize_me_app.views.story_detail'),
    url(r'^story/(?P<pk>\d+)/$',
        DetailView.as_view(model=Story), name='story_detail'),
    url(r'^product/(?P<product_id>\d+)/new_story/', 'scrummetize_me_app.views.new_story'),
    url(r'^product/(?P<product_id>\d+)/add_story/', 'scrummetize_me_app.views.add_story'),
)
